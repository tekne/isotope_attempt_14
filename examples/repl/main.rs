use rustyline::error::ReadlineError;
use rustyline::Editor;
use clap::{Arg, App};

use rain_lang::parser::{symbol_table::SymbolTable, syntax::{parse_val, parse_statement}};

pub mod parser;

fn main() {
    let matches = App::new("rain REPL")
                      .version("0.0")
                      .author("Jad Elkhaleq Ghalayini <jad.ghalayini@mail.utoronto.ca>")
                      .about("Simple REPL for rain IR")
                      .arg(Arg::with_name("history")
                           .short("h")
                           .long("history")
                           .value_name("FILE")
                           .help("A file to store history")
                           .takes_value(true))
                      .get_matches();
    let history = matches.value_of("history").unwrap_or(".rain_history");
    println!("rain REPL 0.0, saving history to {}", history);

    let mut editor = Editor::<()>::new();
    if editor.load_history(history).is_err() {
        println!("No previous history.");
    }

    let mut symbols = SymbolTable::with_capacity(0);
    let mut buffer = String::new();

    loop {
        let line = editor.readline("rain> ");
        match line {
            Ok(line) => {
                buffer.push_str(&line);
                let parsed = parse_val(&symbols)(&buffer);
                match parsed {
                    Ok((rest, val)) => {
                        buffer = String::from(rest); // TODO: try to avoid this allocation...
                        if val.is_err() { print!("ERROR: ") }
                        println!("{}", val)
                    },
                    Err(e) => {
                        let parsed = parse_statement(&mut symbols, &buffer);
                        match parsed {
                            Ok((rest, _)) => buffer = String::from(rest),
                            Err(pe) => {
                                println!("Parsing error: {:?} (value), {:?} (statement)", e, pe);
                                buffer.clear()
                            }
                        }
                    }
                }
            },
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => { break },
            Err(err) => { println!("Error: {}", err); break; }
        }
    }

    editor.save_history(history).unwrap();
}
