/*!
The `rain` intermediate representation
*/

#![feature(weak_into_raw)]

pub mod value;
pub mod parser;
pub mod util;
