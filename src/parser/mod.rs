/*!
Utilities for parsing a simple syntax for `rain` (which is itself syntax-independent).
Useful for testing, repl usage and storage until a binary format is developed.
*/

pub mod symbol_table;
pub mod syntax;
