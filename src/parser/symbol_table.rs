/*!
A simple, linked-list like symbol table module
*/
use internship::IStr;
use std::collections::HashMap;
use std::hash::Hash;

use crate::value::Value;

/// A simple, linked-list like symbol table for `rain` values
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct SymbolTable<'a, K: Eq + Hash = IStr, V=Value> {
    pub symbols: HashMap<K, V>,
    pub stack: Option<&'a SymbolTable<'a, K, V>>
}

impl<K: Eq + Hash, V: Clone> SymbolTable<'_, K, V> {
    /// Push a symbol table onto this symbol table
    pub fn push(&self) -> SymbolTable<'_, K, V> {
        SymbolTable { symbols: HashMap::new(), stack: Some(self) }
    }
    /// Create a new symbol table with a given capacity
    pub fn with_capacity<'a>(n: usize) -> SymbolTable<'static, K, V> {
        SymbolTable { symbols: HashMap::with_capacity(n), stack: None }
    }
    /// Get the definition of a given symbol, without truncating
    #[inline]
    pub fn get(&self, name: &K) -> Option<V> {
        if let Some(symbol) = self.symbols.get(name) { return Some(symbol.clone()) }
        if let Some(stack) = self.stack { stack.get(name) } else { None }
    }
    /// Define a new symbol at the current stack depth.
    /// If one was already defined, return it
    pub fn define(&mut self, name: K, val: V) -> Option<V> {
        self.symbols.insert(name, val)
    }
    /// Delete a given symbol at the current stack depth.
    /// Return its current definition, if it was not already undefined.
    pub fn undef(&mut self, name: &K) -> Option<V> { self.symbols.remove(name) }
}
