use nom::{
    IResult,
    bytes::complete::{tag, is_not},
    character::complete::{multispace0, digit1},
    branch::alt,
    combinator::{map, map_res, map_opt, verify, opt},
    sequence::{delimited, tuple, preceded},
    multi::many0
};
use internship::IStr;
use std::str::FromStr;
use crate::value::{Value, Sexpr, Tuple, Expr};

use super::symbol_table::SymbolTable;

const SPECIAL_CHARS: &str = " \t\r\n:;'\"[](){}%";

/// Parse an identifier
pub fn parse_ident(input: &str) -> IResult<&str, &str> {
    verify(is_not(SPECIAL_CHARS), |s: &str| !s.is_empty())(input)
}

/// Parse a named value given a symbol table
pub fn parse_named<'a>(table: &'a SymbolTable<'a>) -> impl Fn(&str) -> IResult<&str, Value> + '_ {
    move |input| map_opt(parse_ident, |ident| table.get(&IStr::new(ident)))(input)
}

/// Parse an atomic value given a symbol table
pub fn parse_atom<'a>(table: &'a SymbolTable<'a>) -> impl Fn(&str) -> IResult<&str, Value> + '_ {
    move |input| alt((
        parse_op,
        map(parse_bool, |b| Value::Bool(b)),
        parse_named(table),
        map(parse_expr(table), |e| Value::from(e)),
        parse_comp_val(table)
    ))(input)
}

/// Parse a typing judgement
pub fn parse_ty<'a>(table: &'a SymbolTable<'a>) -> impl Fn(&str) -> IResult<&str, Value> + '_ {
    move |input| preceded(
        delimited(multispace0, tag(":"), multispace0),
        parse_val(table)
    )(input)
}

/// Parse a value given a symbol table
pub fn parse_val<'a>(table: &'a SymbolTable<'a>) -> impl Fn(&str) -> IResult<&str, Value> + '_ {
    move |input| map(
        tuple((parse_atom(table), many0(parse_index), opt(parse_ty(table)))),
        |(mut a, ixs, ty)| {
            for ix in ixs { a = Value::elem(ix, a); }
            if let Some(ty) = ty { a.judge_type(ty) }
            a
        }
    )(input)
}

/// Parse a projection index
pub fn parse_index(input: &str) -> IResult<&str, u32> { preceded(tag("::"), parse_u32)(input) }

pub fn parse_u32(input: &str) -> IResult<&str, u32> { map_res(digit1, u32::from_str)(input) }

/// Parse a boolean
pub fn parse_bool(input: &str) -> IResult<&str, bool> {
    alt((map(tag("true"), |_| true), map(tag("false"), |_| false)))(input)
}

/// Parse a boolean operator as a value
pub fn parse_op(input: &str) -> IResult<&str, Value> {
    alt((
        map(tag("&"), |_| Value::And),
        map(tag("|"), |_| Value::Or),
        map(tag("^"), |_| Value::Xor),
        map(tag("!"), |_| Value::Not)
    ))(input)
}

/// Parse an S-expression
pub fn parse_sexpr<'a>(table: &'a SymbolTable<'a>) -> impl Fn(& str) -> IResult<&str, Sexpr> + '_ {
    move |input| map(delimited(
            tag("("),
            many0(delimited(multispace0, parse_val(table), multispace0)),
            tag(")")),
        |mut v| { v.reverse(); Sexpr(v) })(input)
}

/// Parse a tuple
pub fn parse_tuple<'a>(table: &'a SymbolTable<'a>) -> impl Fn(& str) -> IResult<&str, Tuple> + '_ {
    move |input| map(delimited(
            tag("["),
            many0(delimited(multispace0, parse_val(table), multispace0)),
            tag("]")),
        |v| Tuple(v))(input)
}

/// Parse an expression
pub fn parse_expr<'a>(table: &'a SymbolTable<'a>) -> impl Fn(& str) -> IResult<&str, Expr> + '_ {
    move |input| alt((
        map(parse_sexpr(table), |s| Expr::Sexpr(s)),
        map(parse_tuple(table), |t| Expr::Tuple(t))
    ))(input)
}

/// Parse a composite value
pub fn parse_comp_val<'a>(symbols: &'a SymbolTable<'a>)
-> impl Fn(&str) -> IResult<&str, Value> + '_{ move |mut input| {
        input = match delimited(multispace0, tag("{"), multispace0)(input) {
            Ok((rest, _)) => rest, Err(e) => { return Err(e) }
        };
        let mut symbols = symbols.push();
        while let Ok((rest, _symbol)) = parse_statement(&mut symbols, input) {
            input = rest;
        }
        let result = match parse_val(&symbols)(input) {
            Ok((rest, value)) => {
                match delimited(multispace0, tag("}"), multispace0)(rest) {
                    Ok((rest, _)) => Ok((rest, value)), Err(e) => Err(e)
                }
            },
            Err(e) => {
                Err(e)
            }
        };
        result
    }
}

/// Parse a statement
pub fn parse_statement<'a>(symbols: &mut SymbolTable, input: &'a str) -> IResult<&'a str, ()> {
    parse_let(symbols, input)
}

/// Parse a let statement
pub fn parse_let<'a>(symbols: &mut SymbolTable, input: &'a str) -> IResult<&'a str, ()> {
    let res = delimited(
        delimited(multispace0, tag("let"), multispace0),
        parse_ident,
        delimited(multispace0, tag("="), multispace0)
    )(input);
    let (rest, ident) = match res { Ok(res) => res, Err(e) => { return Err(e) } };
    let res = parse_val(symbols)(rest);
    match res {
        Ok((rest, val)) => {
            let rest = match delimited(multispace0, tag(";"), multispace0)(rest) {
                Ok((rest, _)) => rest,
                Err(e) => { return Err(e) }
            };
            let ident = IStr::new(ident);
            symbols.define(ident.clone(), val.clone());
            Ok((rest, ()))
        },
        Err(e) => {
            Err(e)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    pub fn bools_parse_correctly() {
        assert_eq!(parse_bool("true").unwrap(), ("", true));
        assert_eq!(parse_bool("false").unwrap(), ("", false));
    }
    #[test]
    pub fn sexprs_parse_correctly() {
        let symbols = SymbolTable::with_capacity(0);
        assert_eq!(
            parse_sexpr(&symbols)("(& true false)").unwrap(),
            ("", Sexpr(vec![Value::Bool(false), Value::Bool(true), Value::And]))
        );
    }
    #[test]
    pub fn basic_exprs_parse_and_normalize_correctly() {
        let symbols = SymbolTable::with_capacity(0);
        assert_eq!(parse_val(&symbols)("(& true false)").unwrap(), ("", Value::Bool(false)));
        assert_eq!(parse_val(&symbols)("(& true true)").unwrap(), ("", Value::Bool(true)));
        assert_eq!(
            parse_val(&symbols)("(& true)").unwrap(),
            ("", Value::from(Sexpr(vec![Value::Bool(true), Value::And]))));
    }
    #[test]
    pub fn basic_compound_exprs_parse_and_normalize_correctly() {
        let symbols = SymbolTable::with_capacity(0);
        assert_eq!(parse_val(&symbols)("{let x = true; x}").unwrap(), ("", Value::Bool(true)));
        assert_eq!(
            parse_val(&symbols)("{let x = true; let y = false; (& x y)}").unwrap(),
            ("", Value::Bool(false))
        );
        assert_eq!(
            parse_val(&symbols)("{let x = (| true); let y = false; (x y)}").unwrap(),
            ("", Value::Bool(true))
        );
    }
    #[test]
    pub fn basic_statement_parsing_works_correctly() {
        let mut symbols = SymbolTable::with_capacity(1);
        assert_eq!(parse_statement(&mut symbols, "let f = (^ true);").unwrap(), ("", ()));
        assert_eq!(parse_statement(&mut symbols, "let x = true;").unwrap(), ("", ()));
        assert_eq!(symbols.get(&IStr::new("x")), Some(Value::Bool(true)));
        assert_eq!(parse_val(&symbols)("(f x)").unwrap(), ("", Value::Bool(false)));
    }
}
