/*!
Utility functions for use throughout `rain`
*/

/// Get the least number of bits required to represent a given value in an unsigned integer
#[inline] pub fn least_bits_to_represent(value: u64) -> u32 {
    if value == 0 { 0 } else { 64 - (value - 1).leading_zeros() }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn bit_representation_is_correct_for_small_finite() {
        let mut pos = 1;
        let mut bits = 0;
        let max = 10000;
        for i in 0..max {
            assert_eq!(least_bits_to_represent(i), bits);
            if i == pos {
                pos *= 2;
                bits += 1;
            }
        }
    }
}
