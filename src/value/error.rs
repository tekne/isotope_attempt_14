/*!
`rain` error values.
*/

use std::sync::Arc;
use std::fmt::{self, Display, Formatter};
use either::Either;

use super::{Value, Sexpr, Tuple, Lambda, Expr, Node, NodeData};

impl Value {
    /// Check whether this value is an error
    pub fn is_err(&self) -> bool {
        match self {
            Value::Error(_) => true,
            _ => false
        }
    }
    /// Check whether this value contains an error type
    pub fn has_err(&self) -> bool {
        match self {
            Value::Error(_) => true,
            Value::Node(n) => n.has_err(),
            _ => false
        }
    }
}

impl Node {
    /// Check whether this node contains an error
    pub fn has_err(&self) -> bool { self.data().has_err() }
}

impl NodeData {
    /// Check whether this node data contains an error
    pub fn has_err(&self) -> bool {
        // Check for expression errors
        if self.expr.has_err() { return true }
        // Check for type errors
        if let Some(ty) = self.ty.as_ref() { ty.has_err() } else { false }
    }
}

impl Expr {
    /// Check whether this expression contains an error
    pub fn has_err(&self) -> bool {
        match self {
            Expr::Value(v) => v.has_err(),
            Expr::Sexpr(s) => s.has_err(),
            Expr::Tuple(t) => t.has_err(),
            Expr::Lambda(l) => l.has_err(),
            Expr::Parameter(_) => false
        }
    }
}

impl Sexpr {
    /// Check whether this S-expression contains an error
    pub fn has_err(&self) -> bool { self.0.iter().any(|v| v.has_err()) }
}
impl Tuple {
    /// Check whether this tuple contains an error
    pub fn has_err(&self) -> bool { self.0.iter().any(|v| v.has_err()) }
}
impl Lambda {
    /// Check whether this lambda function contains an error
    pub fn has_err(&self) -> bool {
        self.args().iter().any(|v| v.data().has_err()) | match self.ret() {
            None => false,
            Some(Either::Left(tuple)) => tuple.0.iter().any(|v| v.has_err()),
            Some(Either::Right(value)) => value.has_err()
        }
    }
}


/// An invalid `rain` value
#[derive(Debug, Clone)]
pub struct ErrorValue(pub Arc<RainError>);

impl From<ErrorValue> for Value {
    fn from(err: ErrorValue) -> Value { Value::Error(err) }
}

impl PartialEq for ErrorValue {
    fn eq(&self, other: &ErrorValue) -> bool { Arc::ptr_eq(&self.0, &other.0) }
}
impl Eq for ErrorValue {}

impl Display for ErrorValue {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> { self.0.fmt(f) }
}

/// A `rain` error
#[derive(Debug, Clone)]
pub enum RainError {
    /// Attempted to call something that is not a function in an S-expression
    NotAFunction {
        bad_expr: Sexpr
    },
    /// Invalid index into a tuple
    IndexOutOfRange {
        expr: Expr,
        index: u32
    },
    /// Trying to index a non-tuple
    NotATuple {
        value: Value,
        index: u32
    },
    /// Trying to evaluate an undefined lambsa function
    UndefinedLambda
}

impl From<RainError> for Value {
    fn from(err: RainError) -> Value { Value::Error(ErrorValue(Arc::new(err))) }
}

impl Display for RainError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        match self {
            RainError::NotAFunction { bad_expr } => {
                write!(f, "Attempted to call a non-function in expression {}", bad_expr)
            },
            RainError::IndexOutOfRange { expr, index } => {
                write!(f, "Index out of range {} in expression {}", index, expr)
            },
            RainError::NotATuple { value, index } => {
                write!(f, "Attempted to index non-tuple {} with index {}", value, index)
            },
            RainError::UndefinedLambda => {
                write!(f, "Attempted to evaluate an undefined lambda function!")
            }
        }
    }
}
