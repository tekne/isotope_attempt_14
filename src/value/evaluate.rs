/*!
Interpreted evaluation of `rain` lambdas
*/
use super::{Expr, Value, Node, NodeData, Tuple, NodeAddr, Sexpr, RainError};
use std::collections::HashMap;
use std::fmt::{self, Display, Formatter};
use either::Either;


/// An parameter to a lambda expression
#[derive(Debug, Clone)]
pub struct Parameter {
    /// The number of this parameter
    pub number: usize
}

impl Parameter {
    pub fn new(number: usize) -> Parameter { Parameter { number } }
    pub fn node(self, ty: Option<Value>) -> Node {
        Node::from(NodeData { expr: Expr::Parameter(self), ty, scope: None })
    }
}

impl PartialEq for Parameter {
    fn eq(&self, other: &Parameter) -> bool { self.number == other.number }
}
impl Eq for Parameter {}

impl Display for Parameter {
    //TODO: this
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> { write!(f, "${}", self.number) }
}

/// A lambda expression
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Lambda {
    /// Parameters
    args: Vec<NodeAddr>,
    /// Return value
    ret: Either<Tuple, Value>,
    /// Scope level
    scope_level: u32
}

impl Lambda {
    /// Get the arguments of this lambda function
    pub fn args(&self) -> &[NodeAddr] { &self.args }
    /// Get the return value of this lambda function, if any
    pub fn ret(&self) -> Option<&Either<Tuple, Value>> {
        if self.has_ret() { Some(&self.ret) } else { None }
    }
    /// Get whether this lambda function has a return value assigned
    pub fn has_ret(&self) -> bool {
        match &self.ret {
            Either::Left(Tuple(v)) => !v.is_empty(),
            _ => true
        }
    }
    /// Set the return value of this lambda function if it does not already have one
    pub fn set_ret(&mut self, ret: Either<Tuple, Value>) -> Result<(), Either<Tuple, Value>> {
        if self.has_ret() { Err(ret) } else { self.ret = ret; Ok(()) }
    }

    /// Get the scope level of this lambda
    pub fn scope_level(&self) -> u32 {
        if self.has_ret() { self.scope_level } else { std::u32::MAX }
    }

    /// Value for having no return value
    #[inline(always)] fn ret_none() -> Either<Tuple, Value> { Either::Left(Tuple(Vec::new())) }
    /// Create a lambda function with a given number of typeless parameters
    pub fn with_params(n: usize) -> Lambda {
        let mut args = Vec::with_capacity(n);
        for i in 0..n { args.push(Parameter::new(i).node(None).addr()); }
        Lambda {
            args,
            ret: Self::ret_none(),
            scope_level: 0
        }
    }
}

impl Display for Lambda {
    //TODO: this
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        write!(f, "Lambda({} -> {})", self.args.len(), self.ret)
    }
}

impl Value {
    fn substitute(&self, ctx: &mut HashMap<NodeAddr, Value>, level: u32) -> Value {
        match self {
            Value::Node(n) => n.substitute(ctx, level),
            Value::Elem(i, n) => Value::elem(*i, n.substitute(ctx, level)),
            _ => self.clone() // Do nothing for non-node values
        }
    }
}

impl Node {
    fn substitute(&self, ctx: &mut HashMap<NodeAddr, Value>, level: u32) -> Value {
        let addr = self.clone().addr();
        if let Some(value) = ctx.get(&addr) { return value.clone() }
        let data = self.data();
        if data.scope_level() < level { return Value::Node(self.clone()) }
        let val = data.substitute(ctx, level);
        ctx.insert(addr, val.clone());
        val
    }
}

impl NodeData {
    fn substitute(&self, ctx: &mut HashMap<NodeAddr, Value>, level: u32) -> Value {
        let val = self.expr.substitute(ctx, level);
        //TODO: should this be done if val is not a node or elem?
        if let Some(ty) = &self.ty { val.judge_type(ty.clone()) }
        val
    }
}

impl Expr {
    fn substitute(&self, ctx: &mut HashMap<NodeAddr, Value>, level: u32) -> Value {
        match self {
            Expr::Value(v) => v.substitute(ctx, level),
            Expr::Tuple(t) => t.substitute(ctx, level),
            Expr::Sexpr(s) => s.substitute(ctx, level),
            Expr::Lambda(l) => l.substitute(ctx, level),
            // Do nothing for parameters, which are dead ends
            Expr::Parameter(p) => Expr::Parameter(p.clone()).into()
        }
    }
}

impl Tuple {
    fn substitute_def(&self, ctx: &mut HashMap<NodeAddr, Value>, level: u32) -> Tuple {
        Tuple(self.0.iter().map(|val| val.substitute(ctx, level)).collect())
    }
    fn substitute(&self, ctx: &mut HashMap<NodeAddr, Value>, level: u32) -> Value {
        Expr::Tuple(self.substitute_def(ctx, level)).into()
    }
}

impl Sexpr {
    fn substitute_def(&self, ctx: &mut HashMap<NodeAddr, Value>, level: u32) -> Sexpr {
        Sexpr(self.0.iter().map(|val| val.substitute(ctx, level)).collect())
    }
    fn substitute(&self, ctx: &mut HashMap<NodeAddr, Value>, level: u32) -> Value {
        self.substitute_def(ctx, level).normalized().into()
    }
}

impl Lambda {
    fn substitute(&self, ctx: &mut HashMap<NodeAddr, Value>, level: u32) -> Value {
        let args = Vec::from(self.args()); // TODO: change parameter association...
        let ret = match self.ret() {
            None => Either::Left(Tuple(Vec::new())),
            Some(Either::Left(tup)) => Either::Left(tup.substitute_def(ctx, level)),
            Some(Either::Right(val)) => Either::Right(val.substitute(ctx, level))
        };
        //TODO: scope level change?
        Expr::Lambda(Lambda { args, ret, scope_level: 0 }).into()
    }
    fn substitute_eval(&self, ctx: &mut HashMap<NodeAddr, Value>) -> Expr {
        match self.ret() {
            None => Expr::Value(RainError::UndefinedLambda.into()),
            Some(Either::Left(Tuple(vals))) => {
                let vals = vals.iter().map(|val| val.substitute(ctx, self.scope_level));
                Expr::Tuple(Tuple(vals.collect()))
            },
            Some(Either::Right(val)) => Expr::Value(val.substitute(ctx, self.scope_level))
        }
    }
    /// Evaluate a lambda function on a given set of arguments
    pub fn evaluate(&self, args: &[Value]) -> Expr {
        let mut ctx = HashMap::with_capacity(args.len());
        for (arg, val) in self.args().iter().cloned().zip(args.iter().cloned()) {
            ctx.insert(arg, val);
        }
        self.substitute_eval(&mut ctx)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn identity_on_bools_evaluates_properly() {
        let mut id = Lambda::with_params(1);
        id.set_ret(Either::Right(Value::Node(id.args()[0].clone().node()))).unwrap();
        assert_eq!(id.evaluate(&[Value::Bool(true)]), Value::Bool(true).into());
        assert_eq!(id.evaluate(&[Value::Bool(false)]), Value::Bool(false).into());
    }
}
