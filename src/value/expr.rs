/*!
`rain` expressions
*/

use std::fmt::{self, Display, Formatter};
use super::{Value, Node, Lambda, Parameter};

/// A `rain` expression
#[derive(Debug, Clone, Eq, PartialEq, derive_more::From, derive_more::Display)]
pub enum Expr {
    /// Value
    Value(Value),
    /// An S-Expr
    Sexpr(Sexpr),
    /// A tuple
    Tuple(Tuple),
    /// A lambda expression
    Lambda(Lambda),
    /// A parameter to a lambda-like node
    Parameter(Parameter)
}

impl Expr {
    /// Wrap an `Expr` into a `Value`, if necessary
    pub fn wrap(self) -> Value {
        match self {
            Expr::Value(v) => v,
            e => Value::Node(Node::from(e))
        }
    }
}

impl From<Vec<Value>> for Expr {
    fn from(values: Vec<Value>) -> Expr { Expr::Tuple(Tuple(values)) }
}

/// An S-expression
#[derive(Debug, Clone, Eq, PartialEq, derive_more::From)]
pub struct Sexpr(pub Vec<Value>);

impl Sexpr {
    /// Get the function to be evaluated by this sexpr
    pub fn get_function(&self) -> Option<Value> { self.function().cloned() }
    /// Borrow the function to be evaluated by this sexpr
    pub fn function(&self) -> Option<&Value> { self.0.last() }
    /// Borrow the function to be evaluated by this sexpr
    pub fn function_mut(&mut self) -> Option<&mut Value> { self.0.last_mut() }
    /// Borrow the Parameters of this sexpr's function
    pub fn args(&self) -> &[Value] {
        let len = self.0.len();
        let new_len = if len == 0 { 0 } else { len - 1 };
        &self.0[0..new_len]
    }
    /// Mutably borrow the Parameters of this sexpr's function
    pub fn args_mut(&mut self) -> &[Value] {
        let no_args = self.no_args();
        &mut self.0[0..no_args]
    }
    /// Get the length of this S-expression
    pub fn len(&self) -> usize { self.0.len() }
    /// Get the number of Parameters of this S-expression
    pub fn no_args(&self) -> usize { let len = self.len(); if len == 0 { 0 } else { len - 1 } }
    /// Check whether this S-expression is `nil`
    pub fn is_nil(&self) -> bool { self.0.is_empty() }
}

impl Display for Sexpr {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        let mut first = true;
        write!(f, "(")?;
        for elem in self.0.iter() {
            write!(f, "{}{}", if first { "" } else { " " }, elem)?;
            first = false;
        }
        write!(f, ")")
    }
}

/// A tuple
#[derive(Debug, Clone, Eq, PartialEq, derive_more::From, derive_more::Into)]
pub struct Tuple(pub Vec<Value>);

impl Display for Tuple {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        let mut first = true;
        write!(f, "[")?;
        for elem in self.0.iter() {
            write!(f, "{}{}", if first {"" } else { " " }, elem)?;
            first = false;
        }
        write!(f, "]")
    }
}
