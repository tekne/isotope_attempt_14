/*!
`rain` values, which are the fundamental objects of `rain`
*/
use std::fmt::{self, Display, Formatter};

mod expr;
pub use expr::*;
mod normalize;
pub use normalize::*;
mod evaluate;
pub use evaluate::*;
mod error;
pub use error::*;
mod node;
pub use node::*;

/// A `rain` value
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Value {
    /// A node in the `rain` graph
    Node(Node),
    /// An element a tuple-node in the `rain` graph
    Elem(u32, Node),
    /// A Boolean value
    Bool(bool),
    /// The AND function on bits
    And,
    /// The OR function on bits
    Or,
    /// The XOR function on bits
    Xor,
    /// The NOT function on bits
    Not,
    /// The empty tuple
    Nil,
    /// An invalid `rain` value
    Error(ErrorValue)
}

impl Value {
    pub fn elem(i: u32, v: Value) -> Value {
        match v {
            Value::Node(n) => {
                let mut result = Value::Elem(i, n);
                result.normalize();
                result
            },
            Value::Elem(j, n) => {
                let node = Expr::Value(Value::Elem(j, n)).into();
                Value::Elem(i, node)
            },
            value => RainError::NotATuple { value, index: i  }.into()
        }
    }
    pub fn judge_type(&self, ty: Value) {
        match self {
            Value::Node(n) => n.judge_type(ty),
            _ => { /*TODO: check for type errors*/ }
        }
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        use Value::*;
        match self {
            Node(n) => write!(f, "{}", n),
            Elem(i, n) => write!(f, "{}[{}]", n, i),
            Bool(b) => write!(f, "{}", if *b { "true" } else { "false" }),
            And => write!(f, "&"),
            Or => write!(f, "|"),
            Xor => write!(f, "^"),
            Not => write!(f, "!"),
            Nil => write!(f, "()"),
            Error(e) => write!(f, "{}", e)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Value, Sexpr};
    #[test]
    fn bools_print_correctly() {
        assert_eq!(format!("{}", Value::Bool(true)), "true");
        assert_eq!(format!("{}", Value::Bool(false)), "false");
    }
    #[test]
    fn basic_sexprs_print_correctly() {
        assert_eq!(format!("{}", Sexpr(vec![])), "()");
        assert_eq!(format!("{}", Sexpr(vec![Value::Bool(true)])), "(true)");
        assert_eq!(
            format!("{}", Sexpr(vec![Value::And, Value::Bool(true), Value::Bool(false)])),
            "(& true false)"
        );
    }
    #[test]
    fn negation_normalizes_properly() {
        assert_eq!(Value::from(Sexpr(vec![Value::Bool(true), Value::Not])), Value::Bool(false));
        assert_eq!(Value::from(Sexpr(vec![Value::Bool(false), Value::Not])), Value::Bool(true));
    }
    #[test]
    fn binary_operations_normalize_properly() {
        assert_eq!(
            Value::from(Sexpr(vec![Value::Bool(true), Value::Bool(false), Value::And])),
            Value::Bool(false)
        );
        assert_eq!(
            Value::from(Sexpr(vec![Value::Bool(true), Value::Bool(true), Value::And])),
            Value::Bool(true)
        );
        assert_eq!(
            Value::from(Sexpr(vec![Value::Bool(true), Value::Bool(false), Value::Or])),
            Value::Bool(true)
        );
        assert_eq!(
            Value::from(Sexpr(vec![Value::Bool(true), Value::Bool(true), Value::Xor])),
            Value::Bool(false)
        );
    }
    #[test]
    fn nested_binary_operations_work_properly() {
        assert_eq!(
            Value::from(Sexpr(vec![
                Value::Bool(true),
                Sexpr(vec![Value::Bool(false), Value::And]).into()])),
            Value::Bool(false)
        );
    }
}
