use std::hash::{Hash, Hasher};
use std::ops::Deref;
use std::sync::{Arc, Weak};
use std::fmt::{self, Display, Formatter};

use parking_lot::RwLock;
use by_address::ByAddress;

use super::{Expr, Value};

/// A node in the `rain` graph
#[derive(Debug, Clone)]
pub struct Node {
    /// The data of this node
    pub(super) data: Arc<RwLock<NodeData>>
}

/// An address of a node in the `rain` graph
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct NodeAddr {
    pub(super) data: ByAddress<Arc<RwLock<NodeData>>>
}

/// A weak reference to a node in the `rain` graph
#[derive(Debug, Clone, Default)]
pub struct WeakNode {
    /// The data of this node
    pub(super) data: Weak<RwLock<NodeData>>
}

/// The data of a node in the `rain` graph
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct NodeData {
    /// The value associated with this node
    pub expr: Expr,
    /// The type of this node, if any has been assigned
    pub ty: Option<Value>,
    /// The scope this value is associated with, if any
    /// None implies unknown scope, whereas `WeakNode::default()` is root scope.
    pub scope: Option<WeakNode>
}

impl Node {
    /// Get an immutable view into the data of this node
    pub fn data<'a>(&'a self) -> impl Deref<Target=NodeData> + 'a { self.data.read() }
    /// Get the address of this node
    pub fn addr(self) -> NodeAddr { NodeAddr { data: ByAddress(self.data) } }
     /// Downgrade this `Node` into a `WeakNode`
    pub fn downgrade(&self) -> WeakNode { WeakNode { data: Arc::downgrade(&self.data) } }
    /// Set the typing judgement of this node.
    /// May be invalid, this should be caught at type-checking!
    pub fn judge_type(&self, ty: Value) {
        //TODO: check previous type, etc.
        self.data.write().ty = Some(ty);
    }
    /// Get the scope level of this node
    pub fn scope_level(&self) -> u32 {
        self.data().scope_level()
    }
}

impl NodeAddr {
    /// Get an immutable view into the data of this node
    pub fn data<'a>(&'a self) -> impl Deref<Target=NodeData> + 'a { self.data.read() }
    /// Get this node
    pub fn node(self) -> Node { Node { data: self.data.0 } }
    /// Get the scope level of this node
    pub fn scope_level(&self) -> u32 {
        self.data().scope_level()
    }
}

impl WeakNode {
    /// Attempt to upgrade this `WeakNode` into a `Node`
    pub fn upgrade(&self) -> Option<Node> { self.data.upgrade().map(|data| Node { data }) }
    /// Get the scope level of this node
    pub fn scope_level(&self) -> u32 {
        self.upgrade().map(|data| data.scope_level()).unwrap_or(0)
    }
}

impl NodeData {
    /// Get the scope level of this node data
    pub fn scope_level(&self) -> u32 {
        match &self.expr {
            Expr::Lambda(l) => l.scope_level(),
            _ => self.scope.as_ref()
                .map(|scope| scope.scope_level().saturating_add(1))
                .unwrap_or(std::u32::MAX)
        }
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Node) -> bool {
        Arc::ptr_eq(&self.data, &other.data) || self.data().eq(&other.data())
    }
}

impl Eq for Node {}

impl From<Expr> for Node {
    fn from(value: Expr) -> Node { Node::from(NodeData::from(value)) }
}

impl From<NodeData> for Node {
    fn from(data: NodeData) -> Node { Node { data: Arc::new(RwLock::new(data)) } }
}

impl Display for Node {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", self.data.read())
    }
}

impl PartialEq<WeakNode> for NodeAddr {
    fn eq(&self, other: &WeakNode) -> bool {
        (self.data.0.deref()) as *const RwLock<NodeData> == other.data.as_raw()
    }
}

impl PartialEq for WeakNode {
    fn eq(&self, other: &WeakNode) -> bool { Weak::ptr_eq(&self.data, &other.data) }
}

impl PartialEq<NodeAddr> for WeakNode { fn eq(&self, other: &NodeAddr) -> bool { other.eq(self) } }

impl Eq for WeakNode {}

impl Hash for WeakNode {
    fn hash<T: Hasher>(&self, h: &mut T) {
        h.write_usize(self.data.as_raw() as usize)
    }
}

impl From<Expr> for NodeData {
    fn from(expr: Expr) -> NodeData { NodeData { expr, ty: None, scope: None } }
}

impl Display for NodeData {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", self.expr)?;
        if let Some(ty) = self.ty.as_ref() { write!(f, " : {}", ty) } else { Ok(()) }
    }
}
