/*!
Normalization of `rain` values
*/

use std::ops::Deref;

use super::{Sexpr, Tuple, Value, Expr, Node, NodeData, RainError};

impl Sexpr {
    /// Flatten this S-expression: if the function is an S-expression, push it onto the end
    pub fn flatten(&mut self) {
        let node = match self.get_function() {
            Some(Value::Node(n)) => Some(n),
            _ => None
        };
        if let Some(node) = node {
            if let Expr::Sexpr(s) = &node.data().expr {
                self.0.pop();
                self.0.reserve(s.0.len());
                for elem in s.0.iter() { self.0.push(elem.clone()) }
            }
        }
    }
}

pub fn normalize_sexpr(mut expr: Sexpr) -> Expr {

    // Normalize all elements of this sexpr
    for elem in expr.0.iter_mut() { elem.normalize() }

    // Special case: this sexpr has zero or one elements
    if expr.0.len() <= 1 { return Expr::Value(expr.0.pop().unwrap_or(Value::Nil)); }

    // Flatten this S-expression
    expr.flatten();

    let node: Node = match expr.function().unwrap() {
        // Handle builtins
        Value::And => {
            match expr.args() {
                [Value::Bool(l), Value::Bool(r)] => { return Value::Bool(l & r).into() },
                a => if a.len() <= 2 {
                    return Expr::Sexpr(expr)
                } else {
                    return Expr::Value(RainError::NotAFunction { bad_expr: expr }.into())
                }
            }
        },
        Value::Or => {
            match expr.args() {
                [Value::Bool(l), Value::Bool(r)] => { return Value::Bool(l | r).into() },
                a => if a.len() <= 2 {
                    return Expr::Sexpr(expr)
                } else {
                    return Expr::Value(RainError::NotAFunction { bad_expr: expr }.into())
                }
            }
        },
        Value::Xor => {
            match expr.args() {
                [Value::Bool(l), Value::Bool(r)] => { return Value::Bool(l ^ r).into() },
                a => if a.len() <= 2 {
                    return Expr::Sexpr(expr)
                } else {
                    return Expr::Value(RainError::NotAFunction { bad_expr: expr }.into())
                }
            }
        },
        Value::Not => {
            match expr.args() {
                [Value::Bool(b)] => { return Value::Bool(!b).into() },
                a => if a.len() <= 2 {
                    return Expr::Sexpr(expr)
                } else {
                    return Expr::Value(RainError::NotAFunction { bad_expr: expr }.into())
                }
            }
        },
        // Nodes
        Value::Node(n) => n.clone(),
        // Not a function
        _ => { return Expr::Value(RainError::NotAFunction { bad_expr: expr }.into()) }
    };

    // Now that we have a node, lock it and check for lambdas
    let data = node.data();
    match &data.expr {
        Expr::Lambda(l) => l.evaluate(expr.args()),
        _ => Expr::Sexpr(expr)
    }
}

impl Sexpr { pub fn normalized(self) -> Expr { normalize_sexpr(self) } }
impl From<Sexpr> for Value { fn from(expr: Sexpr) -> Value { expr.normalized().wrap() } }

impl Tuple { pub fn normalize(&mut self) { for elem in self.0.iter_mut() { elem.normalize() } } }

impl Expr {
    pub fn normalize(&mut self) {
        match self {
            Expr::Value(v) => v.normalize(),
            Expr::Tuple(t) => t.normalize(),
            Expr::Sexpr(s) => {
                let mut temp = Sexpr(Vec::new()); // Guaranteed no allocation so fast
                std::mem::swap(&mut temp, s);
                *self = normalize_sexpr(temp)
            },
            _ => {}
        }
    }
}
impl From<Expr> for Value { fn from(mut expr: Expr) -> Value { expr.normalize(); expr.wrap() } }

impl Node {
    pub fn normalize<'a>(&'a mut self) -> impl Deref<Target=NodeData> + 'a {
        let mut data = self.data.write();
        data.expr.normalize();
        if let Some(ty) = &mut data.ty { ty.normalize(); }
        data
    }
    pub fn normalized(mut self) -> Value {
        let val = match &self.normalize().expr {
            Expr::Value(v) => Some(v.clone()),
            _ => None
        };
        val.unwrap_or(Value::Node(self))
    }
    pub fn normalized_component(mut self, i: u32) -> Value {
        //TODO: turn panic to Result
        let val = match &self.normalize().expr {
            Expr::Tuple(t) => {
                if i as usize >= t.0.len() {
                    return RainError::IndexOutOfRange{ 
                        expr: Expr::Tuple(t.clone()),
                        index: i
                    }.into();
                }
                Some(t.0[i as usize].clone())
            },
            _ => None
        };
        val.unwrap_or(Value::Node(self))
    }
}

impl Value {
    pub fn normalized(self) -> Value {
        match self {
            Value::Node(n) => n.normalized(),
            Value::Elem(i, n) => n.normalized_component(i),
            v => v
        }
    }
    pub fn normalize(&mut self) {
        let mut temp = Value::Nil;
        std::mem::swap(&mut temp, self);
        *self = temp.normalized()
    }
}
